# Deno Watcher

A file watcher for Deno.

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/deno-watcher/-/blob/main/LICENSE).
Copyright 2022 Binyamin Aron Green.
