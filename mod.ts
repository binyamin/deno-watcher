import { async, EventEmitter } from './deps.ts';

interface FileEvent {
	paths: string[];
}

interface ErrorEvent {
	error: Error;
	// fsEvent: Deno.FsEvent;
	paths: string[];
}

type Events = {
	// This fires on `create`, `remove` and `modify` events
	start: [];
	change: [event: FileEvent];
	error: [event: ErrorEvent];
};

export interface WatcherOptions {
	// /**
	//  * Files under `paths` to ignore
	//  * @todo
	//  */
	// exclude?: string[];

	/**
	 * The debounce time, in milliseconds
	 * @default 100
	 */
	debounce?: number;
}

/**
 * Watch for file system events against one or more paths
 */
export class Watcher extends EventEmitter<Events> {
	#fsWatcher?: Deno.FsWatcher;

	#options: Required<WatcherOptions> & { paths: string | string[] };

	/**
	 * Promise, which resolves whenever the
	 * appropriate file-event should be fired,
	 * and rejects when the watcher closes.
	 */
	#signal = async.deferred();

	// Note: This is cleared when its event is fired
	#changes = new Set<string>();

	/**
	 * @param paths Files or directories to watch
	 */
	constructor(paths: string | string[], options: WatcherOptions = {}) {
		super();

		this.#options = {
			debounce: 100,
			...options,
			paths,
		};
	}

	start() {
		this.emit('start');
		this.#watch();
		this.#run();
	}

	async #run() {
		while (true) {
			await this.#signal;

			try {
				await this.emit('change', {
					paths: Array.from(this.#changes),
				});
			} catch (error) {
				await this.emit('error', {
					error: error instanceof Error ? error : new Error(error),
					paths: Array.from(this.#changes),
				});
			}

			this.#changes.clear();

			// Reset the signal for future events
			this.#signal = async.deferred();
		}
	}

	async #watch() {
		this.#fsWatcher = Deno.watchFs(this.#options.paths);

		// Immediately, begin emitting events
		for await (const fsEvent of this.#fsWatcher) {
			// See https://docs.rs/notify/5.0.0/notify/event/enum.EventKind.html

			if (fsEvent.kind === 'access' || fsEvent.kind === 'other') {
				continue;
			}

			if (fsEvent.kind === 'any') {
				// console.dir(fsEvent); // DEBUGGING
				continue;
			}

			for (const file of fsEvent.paths) {
				this.#changes.add(file);
			}

			// Every time we "debounce", we extend the lifetime of the "signal".
			// Remember, the signal only resolves once we can fire the event
			async.debounce(this.#signal.resolve, this.#options.debounce)();
		}
	}

	/**
	 * Stops watching the file system, and closes the watcher resource.
	 */
	close() {
		if (this.#fsWatcher) this.#fsWatcher.close();
	}
}
