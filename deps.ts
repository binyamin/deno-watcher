export * as path from 'https://deno.land/std@0.160.0/path/mod.ts';

import {
	debounce,
	deferred,
	delay,
} from 'https://deno.land/std@0.160.0/async/mod.ts';
export const async = { debounce, deferred, delay };

export { EventEmitter } from 'https://deno.land/x/event@2.0.1/mod.ts';
