import { async, path } from './deps.ts';
import { Watcher } from './mod.ts';

const watcher = new Watcher('mocks', {
	debounce: 1000,
});

watcher.on('change', (event) => {
	console.log('>>> Changed:');

	for (const p of event.paths) {
		console.log('  - ' + path.relative('.', p));
	}
});

watcher.on('error', (err) => {
	console.debug(err);
});

console.log('Starting watcher...');
// start watching
watcher.start();

// carry on with life, while the watcher works
console.log('Sleeping now...');
await async.delay(5000);
watcher.emit('error', {
	error: new Error('hey!'),
	paths: [],
});
await async.delay(5000);

// stop watching
watcher.close();
console.log('closed');
